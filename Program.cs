﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Vedomost
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> Name = new List<string> { };
            List<string> Surname = new List<string> { };
            List<string> Price = new List<string> { };
            List<string> Date = new List<string> { };
            string strng = null;
            string SumSmbl = null;
            int spaces = 0;
            int maxLongDate = 0;
            int sum = 0;
            int maxSum = 0;
            int index = 0;
            try
            {
                using (StreamReader sr = new StreamReader("Vedomost.txt"))
                {
                    do
                    {
                        strng = sr.ReadLine();
                        spaces = 0;
                        if (strng == null) break;
                        foreach (var symb in strng)
                        {
                            if (symb == ' ')
                            {
                                switch (spaces)
                                {
                                    case 0:
                                        Name.Add(SumSmbl);
                                        break;
                                    case 1:
                                        Surname.Add(SumSmbl);
                                        break;
                                    case 2:
                                        Price.Add(SumSmbl);
                                        break;
                                }
                                spaces++;
                                SumSmbl = null;
                                continue;
                            }
                            SumSmbl += symb;
                        }
                        Date.Add(SumSmbl);
                        SumSmbl = null;
                    } while (strng != null);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            int CountLines = Name.Count;
            for (int i = 0; i < CountLines; i++)
            {
                Console.Write(Name[i] + " " + Surname[i]);
                for (int j = Name[i].Length + Surname[i].Length + 1; j <= 20; j++) { Console.Write(" "); }

                Console.Write(Price[i] + "$");

                for (int j = Price[i].Length; j <= 15; j++) { Console.Write(" "); }
                Console.WriteLine(Date[i]);
            }
            Console.WriteLine();
            for (int i = 0; i < CountLines; i++)
            {
                sum += Convert.ToInt32(Price[i]);
                if (maxLongDate < Date[i].Length) maxLongDate = Date[i].Length;

                if (Convert.ToInt32(Price[i]) > maxSum)
                {
                    maxSum = Convert.ToInt32(Price[i]);
                    index = i;
                }
            }
            Console.WriteLine("Всего выплаченно: " + sum + "$");
            Console.WriteLine("Максимум выплачено " + Name[index] + " " + Surname[index] + " = " + maxSum + "$");
            Console.Read();
        }
    }
}

